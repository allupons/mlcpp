#include "Lua.h"
#include <stdio.h>
#include <dirent.h>
#include <fstream>
#include <streambuf>

/* 
Loads all the scripts in the scripts/ directory and uses their file name (without the .lua) as the function name to call them with
*/

int run(int argc, char* argv[])
{
  printf("Starting Test");
  
  Lua Lua;

  // use the Lua print function to prove Lua is running
  Lua.Call("print", "Lua Loaded", "version_info: mlcpp-0.9 cfb90ae mlcpp 12/02/18-00:17:18", 0);
  
  DIR* dir = opendir("./scripts");

  if (not dir)
    throw Exception(Text("Could not open directory: ."));
  
  for(struct dirent* file = readdir(dir); file; file = readdir(dir)) {
    // skip if is not a lua script
    char* f = strstr(file->d_name, ".lua");
    if (not f)
      continue;
    
    std::ifstream s(Text("./scripts/%s", file->d_name));
    if (not s.good())
      throw Exception(Text("Error reading file: ./scripts/%s", file->d_name));

    // read the entire file into a string (the C++ way)
    std::string str((std::istreambuf_iterator<char>(s)),
		    std::istreambuf_iterator<char>());
    
    *f = '\0'; // put a 0 at the . in the filename to remove the .lua
    
    Lua.Load(str.c_str(), file->d_name);

    // execute the script 10 times to make sure it can do it
    for (int i = 0; i < 10; ++i) {
        printf("Run %d)\nExecuting: %s", i, file->d_name);
        Text rval = Lua.Execute(file->d_name, "parameter1", "parameter2", "parameter3", 0);
        printf("Result of Execute %s is:%s\n", file->d_name, rval.c_str());
        
        printf("Calling: %s", file->d_name);
        rval = Lua.Call(file->d_name, "parameter1", "parameter2", "parameter3", 0);
        printf("Result of Call %s is:%s\n", file->d_name, rval.c_str());
    }
  }

  printf("Ending Test");
  
  closedir(dir);
  
}
  
int main(int argc, char* argv[])
{
  try
    {
      run(argc, argv);
    }
  catch(Exception& e)
    {
      puts(e.what());
      exit(-1);
    }
  catch(std::exception& e)
    {
      puts(e.what());
      exit(-1);
    }
  exit(0);
}
