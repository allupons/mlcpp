#include <string.h>
#include <vector>
#include <map>
#include <exception>
#include <errno.h>
#include <string>

extern "C"
{
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

// tiny exception class
class Exception: public std::exception
{
  std::string error;
  
 public:
  Exception(const char* message, int error_number = errno) {
    error = message;
    if (error_number > 0) {
      char buffer[1024] = "";
      (error += ": ") += strerror_r(error_number, buffer, sizeof(buffer) - 1);
    }
  }
  
  virtual const char* what() {
    return error.c_str();
  }
};

// tiny string class to fix deficiencies in std::string (subclass of std::string)
class Text: public std::string
{
 public:
  operator const char*()
  {
    return this->c_str();
  }
  
  Text& operator=(const char* str) {
    if (not str)
      str = "";
    std::string::operator=(std::string(str));
    return *this;
  }

  Text() {
    operator=("");
  }

  Text(const char *format, ...) // construct string with printf style format string
  {
    size_t bufsize(32000);

    if (not format) {
      operator=("");
      return;
    }
    
    char * const buffer(reinterpret_cast<char *> (::alloca(bufsize)));

    for(;;) {
        va_list args;
        va_start(args, format);
        const int written = ::vsnprintf(buffer, bufsize, format, args);
        
        // If there was insufficient room, set size to what is needed and try again if vsnprintf fails it returns the number of bytes NEEDED so we can
        // try again and succeed
        if (written >= static_cast<int> (bufsize)) {
            va_end(args);
            bufsize = written + 1;
            continue;
        }
        
        if (written < 0)
            throw Exception("in StringUtil::Format: Error formatting Text object.");
        
        break;
    }
    
    operator=(buffer);
  }
    
};

// Lua interpreter class
class Lua {
    
    lua_State* L;
    std::map<Text, Text> Scripts;
    
public:
    void Init() {
        this->L = luaL_newstate();
        
        if (!this->L)
            throw Exception("Could no open lua interpreter");

        luaL_openlibs(L); // this loads Lua's libraries of functions like "print" and "random" and "square root" etc.
    }
    
    Lua() {
        Init();
    }
    
    ~Lua() {
        if (this->L)
            lua_close(this->L);
        
        this->L = 0;
    }

    // loads the lua script, as a string, with the name you will use to call it with
    void Load(const char* code, const char* script_name)
    {
        if (not this->L)
            Init();
        
        if (luaL_loadbuffer(L, code, strlen(code), script_name))
        {
            Text lua_error = lua_tostring(L, -1);
            lua_pop(L, lua_gettop(L));
            throw Exception(Text("Lua Error:%s\nLua couldn't compile\n%s", lua_error.c_str(), script_name));
        }
        
        lua_setglobal(L, script_name);
    }

    // execute the script with parameters, put 0 as the last parameter to tell the function you are done with parameters see the scripts/sprint.lua
    // for how to access the parameters
    Text Execute(const char* script_name, ...)
    {
      if (not L)
	Init();

      // get the previously loaded script to the top of the stack
        lua_getglobal(L, script_name);
	if (lua_isnil(L, -1))
	  throw Exception(Text("%s script not loaded", script_name));

	// push arguments onto the stack
        va_list args;
        va_start(args, script_name);
        int arg_count(0);
        
        for(;;) {
            const char* arg = va_arg(args, const char*);
            if (not arg)
                break;
            lua_pushstring(L, arg);
            arg_count++;
        }
        va_end(args);
	
	if (lua_pcall(L, arg_count, 1, 0))
	  {
	    Text lua_error = lua_tostring(L, -1);
	    lua_pop(L, lua_gettop(L));
	    throw Exception(Text("Lua Error:%s", lua_error.c_str()));
	  }
        
        Text rval = lua_tostring(L, -1);
        
	lua_pop(L, lua_gettop(L));
        
        return rval;
    }    

    // Basically a duplicate of Execute. At one time these were more different but not so much anymore
    Text Call(const char* function, ...)
    {
        if (not this->L)
            Init();
        
        lua_getglobal(L, function);
        
        if (not lua_isfunction(L, -1)) {
            lua_pop(L, lua_gettop(L));
            throw Exception(Text("%s is not a Lua function", function));
        }
        
        va_list args;
        va_start(args, function);
        int arg_count(0);
        
        for(;;) {
            const char* arg = va_arg(args, const char*);
            if (not arg)
                break;
            lua_pushstring(L, arg);
            arg_count++;
        }
        
        va_end(args);
        
        if (lua_pcall(L, arg_count, 1, 0)) {
            Text lua_error = lua_tostring(L, -1);
            lua_pop(L, lua_gettop(L));
            throw Exception(Text("Lua Function: %s failed: %s", function, lua_tostring(L, -1)));
        }		   
        
        Text rval = lua_tostring(L, -1);
        
        lua_pop(L, lua_gettop(L));
        
        return rval;
    }
};


