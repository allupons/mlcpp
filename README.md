# mlcpp

Easily Embed Lua in C++

mlcpp

Demonstrate Embedding Lua in C++ programs

Lua.h contains all the classes

If you don't use Docker you'll have to manually install the necessary
libraries etc.

Docker is sort of an install program and lightweight Virtual Machine
all in one and make setting up environments trivial.

See Dockerfile.dev RUN commands to see what you'll have to install if you don't use Docker

To set up environment

With Docker:

make dbuild    # this will create an environment with all the necessary libraries and tools
make dshell    # this will put you into that environment
make mlcpp     # this will build mlcpp, a test program that uses Lua.h
./mlcpp        # this runs mlcpp

Put your Lua scripts in the scripts/ directory

Instantiate the Lua class (see mlcpp.cc) for an example how to do that.

Call the script as necessary using Lua::Execute or Lua::Call

Currently there is no difference between Execute and Call. They
converged as I was developing this. One may have to be removed if
there is no reason for its existence

Currently you can only pass strings as parameters and return a string
as a function return.

As time permits I'll enhance it to allow passing of any argument type (if possible)

A very good primer on embedding Lua in C: http://troubleshooters.com/codecorn/lua/lua_c_calls_lua.htm







