export CPLUS_INCLUDE_PATH = /usr/include/lua5.2/:.
export LIBRARY_PATH = /usr/lib/x86_64-linux-gnu
export VPATH = $(CPLUS_INCLUDE_PATH):$(LIBRARY_PATH)

DEBUG = 1
CXXFLAGS = -std=c++11
RELEASE_OPTS = -O3
DEBUG_OPTS = -g -pg -O1

ifdef DEBUG 
      	export CXXFLAGS := $(DEBUG_OPTS) $(CXXFLAGS)
else
	export CXXFLAGS := $(RELEASE_OPTS) $(CXXFLAGS)
endif

# generic make-executable from modules
%:
	echo "MAKING $@ from $^"
	$(CXX) $(CXXFLAGS) -fPIC -o $@ $(filter-out Makefile%, $^) -Wl,-rpath,.

# generic make DLL rule
%.so:%.o
	echo "MAKING $@ from $^"
	$(CXX) $(CXXFLAGS) -Wl,--export-dynamic -fPIC -shared -o $@ $(filter %.o, $^)  $(filter %.a, $^)  $(filter %.so, $^)

%.o:%.cc
	echo "MAKING $@ from $^"
	$(CXX) $(CXXFLAGS) -fPIC -c -o $@  $(filter %.cc, $^)

# Docker file make commands

dhelp:
	@echo "Please use 'make <target> ROLE=<ROLE> if you don't specify role, the default will be \"$(ROLE)\" and will use Dockerfile.$(ROLE)"
	@echo "where <target> is one of"
	@echo "Please use 'make <target> ROLE=<role>' where <target> is one of"
	@echo "  dbuild	    build the docker image containing a redis cluster"
	@echo "  drebuild	  rebuilds the image from scratch without using any cached layers"
	@echo "  drun	      run the built docker image"
	@echo "  drestart	  restarts the docker image"
	@echo "  dbash	     starts bash inside a running container."
	@echo "  dclean	    removes the tmp cid file on disk"
	@echo 'equivalent commands may exist for docker compose container but they start with "c" like "make cbash"'
	@echo -n "and <ROLE> is a suffix of a Dockerfile in this directory, one of these: "
	@ls Dockerfile.*
	@echo "Example: make ROLE=dev drun"

dbuild:
	@echo "Building docker image..."
	docker build -f ${DOCKERFILE} -t ${IMAGE_NAME} .

drebuild:
	@echo "Rebuilding docker image using: "
	docker build --rm=true -f ${DOCKERFILE} --no-cache=true -t ${IMAGE_NAME} .

dsetup:
	mkdir -p $(PERSISTENT_DATA_DIR)
	mkdir -p $(PERSISTENT_LOG_DIR)
	mkdir -p $(PERSISTENT_CONFIG_DIR)
	mkdir -p $(PERSISTENT_BIN_DIR)

drun:	dsetup
	@echo "Running docker image..."  # you must stop and rm any container of the same name docker run will fail
	@echo "HOSTARG: $(HOSTARG)"
	docker run --rm $(VOLUMES) $(PORTS) $(HOSTS) $(HOSTARG) -it --name $(IMAGE_NAME) $(IMAGE_NAME) $(CMD)

drestart: stop
	-docker rm ${IMAGE_NAME} 2>/dev/null
	mkdir -p $(PERSISTENT_DATA_DIR)
	mkdir -p $(PERSISTENT_LOG_DIR)
	mkdir -p $(PERSISTENT_CONFIG_DIR)
	mkdir -p $(PERSISTENT_BIN_DIR)
	docker run --rm $(VOLUMES) $(PORTS) $(HOSTS) $(HOSTARG) -it --name ${IMAGE_NAME} ${IMAGE_NAME} $(CMD)

dshell:
	docker run $(VOLUMES) $(PORTS) $(HOSTS) -i -t ${IMAGE_NAME} /bin/bash

dbash:
	docker exec -it ${IMAGE_NAME} /bin/bash

dstop:
	-@docker stop ${IMAGE_NAME}

dclean:
	 # Cleanup cidfile on disk
	 -rm $(CID_FILE) 2>/dev/null

### DOCKER
dockertag:
	docker tag ${IMAGE_NAME} ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}

dockerpush:
	make dockertag
	docker push ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}

