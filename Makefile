include general.mak

.PHONY: version debug

mlcpp: mlcpp.o liblua5.2.a libdl.so

mlcpp.o: mlcpp.cc

clean:
	-rm *.o
	-rm mlcpp

# Docker file make commands

PRODUCT = mlcpp
VERSION = 0.9
ROLE_DEFAULT = dev
ROLE := $(ROLE_DEFAULT)
DOCKERFILE = Dockerfile.$(ROLE)
IMAGE_NAME = $(PRODUCT)_$(ROLE)
IMAGE_TAG ?= latest
PERSISTENT_ROOT = /devel/$(PRODUCT)/$(VERSION)
PERSISTENT_DATA_DIR = var
PERSISTENT_LOG_DIR = log
PERSISTENT_CONFIG_DIR = etc
PERSISTENT_BIN_DIR = bin
VOLUMES = \
	-v $(CURDIR):/source \
	-v $(CURDIR)/$(PERSISTENT_DATA_DIR):$(PERSISTENT_ROOT)/$(PERSISTENT_DATA_DIR) \
	-v $(CURDIR)/$(PERSISTENT_LOG_DIR):$(PERSISTENT_ROOT)/$(PERSISTENT_LOG_DIR) \
	-v $(CURDIR)/$(PERSISTENT_CONFIG_DIR):$(PERSISTENT_ROOT)/$(PERSISTENT_CONFIG_DIR) \
	-v $(CURDIR)/$(PERSISTENT_BIN_DIR):$(PERSISTENT_ROOT)/$(PERSISTENT_BIN_DIR)
HOSTARG = -h $(shell hostname)
HOME = $(shell pwd)
PORTS =
HOSTS =

