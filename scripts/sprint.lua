
local arguments = {...}

print(string.format("number of arguments: %d", #arguments))

for i,v in ipairs(arguments) do
    print(string.format("%d = %s", i, v))
end

return string.format("return number of arguments: %d", #arguments)


